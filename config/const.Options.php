<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * The constants defined in this file change the behaviour of the 
   * framework and the application. You can change the values of the 
   * constants according to the instructions given in comments, or add 
   * new constants that you use in your application.
   */

  /**
   * This is the first page of the application. The framework looks
   * for it at the template folder (specified by TPL). 
   */
define("FIRSTPAGE", "main/main.html");

/**
 * if this constant is true, the framework will load the DB component
 * and will open a default connection with the db specified in the
 * file 'config/const.DB.php'
 */
define("USES_DB", true);

/**
 * Define the Data Source Name that is used by MDB2 to connect
 * to the database, and the MDB2 options for the default connection.
 */
define('MDB2_DSN', "mysql://hsmng:hsmngpass@localhost/hsmanager?new_link=true");
$MDB2_OPTIONS = array(
                      'debug'       => 2,
                      'portability' => MDB2_PORTABILITY_ALL,
                      );


/**
 * this constant defines the type of DB that the application uses
 * it can be: "MySQL", "Oracle", "ODBC", "ProgreSQL", etc.
 * (except "MySQL", the others are not implemented yet)
 */
define("DB_TYPE", "MySQL");

/**
 * This constant is the value returned by the framework 
 * for a DB variable that has a NULL value. It can be
 * "", "NULL", NULL, etc.
 */
define("NULL_VALUE", " ");

/**
 * This constant sets the format of the error message that is displayed
 * when a {{variable}} is not found. 'var_name' is replaced by
 * the actual variable name. Examples: "'var_name' is not defined",
 * "", "undefined", etc. It cannot contain "{{var_name}}" inside.
 */
define("VAR_NOT_FOUND", "{var_name}");

/**
 * When this constant is true, then the CGI vars are displayed
 * at the URL window of the browser. See also SHOW_EXTERNAL_LINK
 * at const.Debug.php.
 */
define("DISPLAY_CGI_VARS", false);

/**
 * The constants LNG and CODESET set a default language and codeset for
 * the application. They are used for the localization (translation)
 * of the messages. They can be changed by calling:
 *    $l10n->set_lng($lng, $codeset)
 * where $l10n is a global variable and $codeset is optional.
 * LNG can be something like 'en_US' or 'en' or UNDEFINED.
 * CODESET can be UNDEFINED, 'iso-latin-1', etc.
 */
//define('LNG', 'sq_AL');
define('LNG', 'en');
define('CODESET', 'iso-8859-1');

/** if true, then use the php-gettext instead of GNU gettext */
define('USE_PHP_GETTEXT', true);

/**
 * The file that contains additional configuration options,
 * which are specific for the application.
 */
define("APP_CONFIG_FILE", "hsmanager.cfg");

//define the application configuration options
//which are declared in the file APP_CONFIG_FILE
if (file_exists(APP_CONFIG_FILE))
  {
    $arr_lines = file(APP_CONFIG_FILE);
    for ($i=0; $i < sizeof($arr_lines); $i++)
      {
        $line = trim($arr_lines[$i]);
        if ($line=='')      continue;
        if ($line[0]=='#')  continue;
        list($const, $value) = split('=', $line, 2);

        //trim any quotation marks (') or (") from $value
        $len = strlen($value);
        if ( ($value[0]=="'" and $value[$len-1]=="'")
             or ($value[0]=='"' and $value[$len-1]=='"') )
          {
            $value = substr($value, 1, $len-2);
          }
        define($const, $value);
      }
  }

//etc.
?>
