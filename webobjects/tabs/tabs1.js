//-*- mode: C; -*-//tells emacs to use mode C for this file
/* This file is part of HotSpot Manager. */

function tabs1_select(obj_id, item) 
{
  SendEvent(obj_id, "select", "item="+item);
}
