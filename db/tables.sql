-- MySQL dump 10.11
--
-- Host: localhost    Database: hsmanager
-- ------------------------------------------------------
-- Server version	5.0.45

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hsmanager`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hsmanager` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `hsmanager`;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `domain` varchar(20) default NULL,
  `username` varchar(20) NOT NULL default '',
  `password` varchar(100) default '',
  `firstname` varchar(20) default NULL,
  `lastname` varchar(20) default NULL,
  `email` varchar(50) default NULL,
  `phone1` varchar(20) default NULL,
  `phone2` varchar(20) default NULL,
  `address` varchar(100) default NULL,
  `notes` text,
  `service` varchar(20) default NULL,
  `expiration_time` datetime default NULL,
  `download_limit` int(11) default NULL,
  `upload_limit` int(11) default NULL,
  `online_time` time default NULL,
  `last_modified` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified_by` varchar(20) default NULL,
  PRIMARY KEY  (`username`),
  UNIQUE KEY `u` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains` (
  `id` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) default NULL,
  `services` varchar(250) default NULL,
  `max_nas_nr` int(11) default NULL,
  `max_client_nr` int(11) default NULL,
  `login_page` varchar(100) default NULL,
  `allowed_sites` text,
  `radius_secret` varchar(64) default NULL,
  `notes` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` varchar(10) NOT NULL default '',
  `name` varchar(100) NOT NULL default '',
  `charset` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user` varchar(20) NOT NULL default '',
  `domain` varchar(20) NOT NULL default '',
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `event` varchar(100) NOT NULL default '',
  `details` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=560 DEFAULT CHARSET=latin1;

--
-- Table structure for table `naslist`
--

DROP TABLE IF EXISTS `naslist`;
CREATE TABLE `naslist` (
  `id` varchar(20) NOT NULL default '',
  `name` varchar(20) default NULL,
  `type` varchar(20) default NULL,
  `domain` varchar(20) default NULL,
  `description` varchar(100) default NULL,
  `mac` varchar(20) default NULL,
  `ip` varchar(20) default NULL,
  `gateway` varchar(20) default NULL,
  `dns1` varchar(20) default NULL,
  `dns2` varchar(20) default NULL,
  `radius_secret` varchar(20) default NULL,
  `phone` varchar(20) default NULL,
  `address` varchar(250) default NULL,
  `latitude` varchar(20) default NULL,
  `longitude` varchar(20) default NULL,
  `notes` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` varchar(20) NOT NULL default '',
  `name` varchar(20) default NULL,
  `description` varchar(100) default NULL,
  `download_rate` int(11) default NULL,
  `upload_rate` int(11) default NULL,
  `online_time` time default NULL,
  `download_limit` int(11) default NULL,
  `upload_limit` int(11) default NULL,
  `notes` varchar(250) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` varchar(255) NOT NULL default '',
  `vars` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(20) default NULL,
  `password` varchar(20) default NULL,
  `firstname` varchar(20) default NULL,
  `lastname` varchar(20) default NULL,
  `phone1` varchar(20) default NULL,
  `phone2` varchar(20) default NULL,
  `email` varchar(100) default NULL,
  `address` varchar(100) default NULL,
  `notes` text,
  `modules` varchar(250) default NULL,
  `domains` varchar(250) default NULL,
  PRIMARY KEY  USING BTREE (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2008-03-01  8:53:45
