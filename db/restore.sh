#!/bin/bash
### restore the database from a backup file
### it must be compressed and have the extension .gz

if [ "$1" = "" ]
then
  echo "Usage: $0 fname"
  echo "fname must be compressed with gzip (having the extension .gz)"
  exit 1
fi

fname_gz=$1
fname=${1%.gz}

### go to this directory
cd $(dirname $0)

### get the db connection variables
. ../hsmanager.cfg

### uncompress the backup file
gunzip backup/$fname_gz

### restore the database from it
mysql --host="$appdb_host" --user="$appdb_user" --password="$appdb_pass" \
      --database="$appdb_name" < backup/$fname

### compress the backup file again
gzip backup/$fname

