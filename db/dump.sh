#!/bin/bash
### dump the DB tables of the module

### go to this directory
cd $(dirname $0)

### get the db connection variables
. ../hsmanager.cfg

### dump the database into the file dbname.sql
mysqldump --add-drop-table --allow-keyword \
          --complete-insert --extended-insert \
          --host="$appdb_host" \
          --user="$appdb_user" \
          --password="$appdb_pass" \
          --databases "$appdb_name" > database.sql

### dump the database structure into the file dbname-tables.sql
mysqldump --add-drop-table --allow-keyword --no-data \
          --host="$appdb_host" \
          --user="$appdb_user" \
          --password="$appdb_pass" \
          --databases "$appdb_name" > tables.sql

### create the database with the command:
### $ mysql -p -u $user -D $database < database.sql

