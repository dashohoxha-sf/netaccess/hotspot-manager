#!/bin/bash
### create a backup file and compress it (.gz)

if [ "$1" = "" ]
then
  echo "Usage: $0 fname"
  exit 1
fi

fname=$1

### go to this directory
cd $(dirname $0)

### get the db connection variables
. ../hsmanager.cfg

### backup the database into a file
mysqldump --add-drop-table --allow-keyword --complete-insert \
          --host="$appdb_host" \
          --user="$appdb_user" \
          --password="$appdb_pass" \
          --databases "$appdb_name" > backup/$fname

### compress
rm -rf backup/$fname.gz
gzip backup/$fname

