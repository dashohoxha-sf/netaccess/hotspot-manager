#!/bin/bash
### apply the SQL API to the radius database

### go to this directory
cd $(dirname $0)

### get the variables for connecting to the radius database
. ../hsmanager.cfg

#debug=/dev/stderr
debug=

### apply the SQL API
cat api.sql \
  | sed -e "s/raddb_name/$raddb_name/g" \
  | tee $debug \
  | mysql --host="$raddb_host"           \
          --user="$raddb_adminuser"      \
          --password="$raddb_adminpass"  \
          --database="$raddb_name"

### apply the access rights
cat access.sql \
  | sed -e "s/raddb_name/$raddb_name/g"        \
        -e "s/raddb_apiuser/$raddb_apiuser/g"  \
        -e "s/raddb_apipass/$raddb_apipass/g"  \
        -e "s/raddb_allowed_hosts/$raddb_allowed_hosts/g" \
  | tee $debug \
  | mysql --host="$raddb_host"           \
          --user="$raddb_adminuser"      \
          --password="$raddb_adminpass"  \
          --database="$raddb_name"

### regenerate DB connection parameters for the application
cat <<EOF > DB.php
<?php
/**
 * Parameters that are used to connect to the database of radius.
 * Generated automatically from 'apply.sh'.
 */
define('RADDB_HOST', '$raddb_host');
define('RADDB_USER', '$raddb_apiuser');
define('RADDB_PASS', '$raddb_apipass');
define('RADDB_NAME', '$raddb_name');
?>
EOF

### apply also the SQL API for RM
if [ "$integrate_with_rm" = "true" ]
then
  ./apply-rm.sh
fi