#!/bin/bash
### apply the SQL API for RM to the radius database

### go to this directory
cd $(dirname $0)

### get the variables for connecting to the radius database
. ../hsmanager.cfg

#debug=/dev/stderr
debug=

### apply the SQL API for RM
cat api-rm.sql \
  | sed -e "s/raddb_name/$raddb_name/g" \
  | tee $debug \
  | mysql --host="$raddb_host"           \
          --user="$raddb_adminuser"      \
          --password="$raddb_adminpass"  \
          --database="$raddb_name"

### apply the access rights for the RM procedures
cat access-rm.sql \
  | sed -e "s/raddb_name/$raddb_name/g"        \
        -e "s/raddb_apiuser/$raddb_apiuser/g"  \
        -e "s/raddb_allowed_hosts/$raddb_allowed_hosts/g" \
  | tee $debug \
  | mysql --host="$raddb_host"           \
          --user="$raddb_adminuser"      \
          --password="$raddb_adminpass"  \
          --database="$raddb_name"
