
/**
 * Set the delimiter of the SQL commands to double semicolon,
 * because semicolon needs to be used inside the procedure declaration
 * to separate the statements.
 */
DELIMITER ;;

/**
 * Select the database that will be used by the procedures and functions.
 * The procedures and functions will be attached to this database.
 */
USE raddb_name ;;


/**  procedure user_save
 * Takes the parameters:  username, password, service, domain
 * In case that such a user exist, it is deleted first, and then
 * new records about the user are inserted.
 */
DROP PROCEDURE IF EXISTS user_save ;;
CREATE PROCEDURE user_save(p_username varchar(64),
                           p_password varchar(253),
                           p_service  varchar(64),
                           p_domain   varchar(20) )
BEGIN
  ### first delete it, in case that such a user exists
  CALL user_del(p_username);

  INSERT INTO radcheck
    (username, attribute, op, Value)
  VALUES
    (p_username, 'User-Password',    ':=', p_password),
    (p_username, 'Simultaneous-Use', ':=', '1'),
    (p_username, 'Huntgroup-Name',   '==', p_domain);

  INSERT INTO usergroup
    (username, groupname)
  VALUES
    (p_username, p_service);
END
;;


/**  function user_check
 * Used to check whether a user already exists in radiusdb
 * (in  the table radcheck). If there is such a user, then
 * it returns its username.
 */
DROP FUNCTION IF EXISTS user_check ;;
CREATE FUNCTION user_check(p_username VARCHAR(64)) 
    RETURNS VARCHAR(64)
    READS SQL DATA
BEGIN
  DECLARE v_username VARCHAR(64) DEFAULT '-';

  SELECT DISTINCT username INTO v_username 
  FROM radcheck WHERE username = p_username;

  RETURN v_username;
END
;;


/**  procedure user_get
 * Returns the data of a given user.
 * Parameters are username and service patterns.
 * Matching is done with LIKE. 
 * The records that are returned have the fields:
 *   username, service
 */
DROP PROCEDURE IF EXISTS user_get ;;
CREATE PROCEDURE user_get(p_username varchar(64),
                          p_service  varchar(64) )
BEGIN
  SELECT radcheck.username AS username,
         usergroup.groupname AS service,
         radcheck.value AS AP_list
  FROM radcheck LEFT JOIN usergroup USING (username)
  WHERE radcheck.username LIKE p_username 
    AND radcheck.attribute = 'NAS-Identifier'
    AND usergroup.groupname LIKE p_service;
END
;;


/**  procedure user_del
 * Delete the given user.
 */
DROP PROCEDURE IF EXISTS user_del ;;
CREATE PROCEDURE user_del(p_username varchar(64))
BEGIN
  DELETE FROM radcheck WHERE username = p_username;
  DELETE FROM usergroup WHERE username = p_username;
END
;;


/**  procedure service_save
 * Save (add or update) a service.
 * Parameters are these: service_name, download_rate, upload_rate
 * Download and upload rates are integers in Kbps.
 * If a service with such a name already exists, it is deleted first.
 */
DROP PROCEDURE IF EXISTS service_save ;;
CREATE PROCEDURE service_save(p_service_name  varchar(64),
                              p_download_rate int(11),
                              p_upload_rate   int(11))
BEGIN
  ### if such a service exists, delete it first
  CALL service_del(p_service_name);

  ### insert into radgroupcheck a record that allways matches
  INSERT INTO radgroupcheck 
    (groupname, attribute, op, value)
  VALUES
    (p_service_name, 'User-Name', '=*', 'username');

  ### insert into radgroupreply the download/upload limits 
  INSERT INTO radgroupreply 
    (groupname, attribute, op, value)
  VALUES
    (p_service_name, 'WISPr-Bandwidth-Max-Down', ':=', p_download_rate * 1024),
    (p_service_name, 'WISPr-Bandwidth-Max-Up', ':=', p_upload_rate * 1024);
END
;;


/**  procedure service_del
 * Delete the service with the given name.
 */
DROP PROCEDURE IF EXISTS service_del ;;
CREATE PROCEDURE service_del(p_service varchar(64))
BEGIN
  DELETE FROM radgroupcheck WHERE groupname = p_service;
  DELETE FROM radgroupreply WHERE groupname = p_service;
END
;;


/**  procedure service_get
 * Return a list of services that match the given parameter.
 * Matching is done with LIKE.
 * The result that is returned contains these fields:
 *   service, download_rate, upload_rate
 * 'download_rate' and 'upload_rate' are integers of Kbps
 */
DROP PROCEDURE IF EXISTS service_get ;;
CREATE PROCEDURE service_get(p_service_name varchar(64))
BEGIN
  SELECT radgroupcheck.groupname AS service,
         (downrate.value DIV 1024) AS download_rate,
         (uprate.value DIV 1024) AS upload_rate
  FROM radgroupcheck LEFT JOIN radgroupreply AS downrate USING (groupname)
                     LEFT JOIN radgroupreply AS uprate USING (groupname)
  WHERE radgroupcheck.groupname LIKE p_service_name
    AND downrate.attribute = 'WISPr-Bandwidth-Max-Down'
    AND uprate.attribute = 'WISPr-Bandwidth-Max-Up';
END
;;


/** procedure change_service_name
 * Changes the name of a service, so that all the clients
 * that were using the old service now use the new service.
 */
DROP PROCEDURE IF EXISTS change_service_name ;;
CREATE PROCEDURE change_service_name(p_old_service varchar(64),
                                     p_new_service varchar(64))
BEGIN
  ### update table usergroup
  UPDATE usergroup 
  SET groupname = p_new_service
  WHERE groupname = p_old_service;

  ### update table radgroupcheck
  UPDATE radgroupcheck
  SET groupname = p_new_service
  WHERE groupname = p_old_service;

  ### update table radgroupreply
  UPDATE radgroupreply
  SET groupname = p_new_service
  WHERE groupname = p_old_service;
END
;;


/** Set the delimiter of the SQL commands back to semicolon. */
DELIMITER ;

