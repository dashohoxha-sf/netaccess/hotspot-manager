

* procedure user_save(p_username varchar(64),
                      p_password varchar(253),
                      p_service  varchar(64),
                      p_domain   varchar(20) )

  Takes the parameters:  username, password, service, domain
  In case that such a user exist, it is deleted first, and then
  new records about the user are inserted.

  Examples:
    + call radius.user_save('user-1','passw-1','test-1','domain-1');
        -- create the user 'user-1' which has access in 'domain-1'
    + call radius.user_save('user-1','xyz','test-1',')');
        -- change the password of 'user-1'


* function user_check(p_username varchar(64)) returns varchar(64)

  Used to check whether a user already exists in radiusdb
  (in  the table radcheck). If there is such a user, then
  it returns its username.

 Examples:
    + select radius.user_check('user-1') as username;
        +----------+
        | username |
        +----------+
        | user-1   |
        +----------+
    + select radius.user_check('user-2') as username;
        +----------+
        | username |
        +----------+
        |          |
        +----------+


* procedure user_get(p_username varchar(64),
                     p_service  varchar(64) )

  Returns the data of a given user.
  Parameters are username and service patterns.
  Matching is done with LIKE. 

  The records that are returned have the fields:
    username, service

  Examples:
    + call user_get('user-1', '%');
        -- get the data of 'user-1'
    + call user_get('%', 'service-1');
        -- get the data of all the users that have the service 'service-1'
    + call user_get('%', '%');
        -- get the data of all the users



* procedure user_del(p_username varchar(64))

  Delete the given user.

  Examples:
    + call radius.user_del('user-2');
        -- delete user 'user-2'



* procedure service_save(p_service_name  varchar(64),
                         p_download_rate int(11),
                         p_upload_rate   int(11))

  Save (add or update) a service.
  Takes the parameters: service_name, download_rate, upload_rate
  Download and upload rates are integers in Kbps.
  If a service with such a name already exists, it is deleted first.

  Examples:
    + call radius.service_save('test-1', 256, 128);
        -- create the service test-1 with 256Kbps download and 128Kbps upload
    + call radius.service_save('test-2', 512, 128);
        -- add another service
    + call radius.service_save('test-2', 512, 256);
        -- change the upload rate of the service test-2



* procedure service_get(p_service_name varchar(64))

  Return a list of services that match the given parameter.
  Matching is done with LIKE.
  The result that is returned contains the fields:
    service, dwmload_rate, upload_rate
  where the rates are integers of Kbps.

  Examples:
    + call radius.service_get('test-1');
        -- get the data of the service 'test-1'
    + call radius.service_get('%');
        -- get the data of all the services
        +----------+---------------+-------------+
        | service  | download_rate | upload_rate |
        +----------+---------------+-------------+
        | test-1   |           256 |         128 | 
        | test-2   |           512 |         256 | 
        +----------+---------------+-------------+



* procedure service_del(p_service varchar(64))

  Delete the service with the given name.

  Examples:
    + call radius.service_del('test-2');
        -- delete the service that is named 'test-2'


* procedure change_service_name(p_old_service varchar(64)
                                p_new_service varchar(64))

  Changes the name of a service, so that all the clients
  that were using the old service now use the new service.

  Examples:
    + call radius.change_service_name('test-2', 'test2');
        -- change the name of the service 'test-2' to 'test2'
