
/**
 * Set the delimiter of the SQL commands to double semicolon,
 * because semicolon needs to be used inside the procedure declaration
 * to separate the statements.
 */
DELIMITER ;;

/**
 * Select the database that will be used by the procedures and functions.
 * The procedures and functions will be attached to this database.
 */
USE raddb_name ;;


/**  procedure rm_user_save
 * Save a user in the table rm_users of the Radius Manager.
 * Takes these parameters: 
 *   username, password, service_id, expiration_date, fullname, email
 * In case that such a user exist, it is deleted first, and then
 * new records about the user are inserted.
 */
DROP PROCEDURE IF EXISTS rm_user_save ;;
CREATE PROCEDURE rm_user_save(p_username        varchar(32),
                              p_password        varchar(32),
                              p_service_id      int(11),
                              p_expiration_date date,
                              p_fullname        varchar(30),
                              p_email           varchar(50))
BEGIN
  ### first delete it, in case that such a user exists
  CALL rm_user_del(p_username);

  ### insert a record into the table rm_users
  INSERT INTO rm_users
  SET
      username    = p_username,
      password    = MD5(p_password),
      srvid       = p_service_id,
      expiration  = p_expiration_date,
      fullname    = p_fullname,
      email       = p_email,
      enableuser  = '1',
      createdon   = NOW(),
      createdby   = 'hsmanager';
END
;;


/**  procedure rm_user_del
 * Delete the given user.
 */
DROP PROCEDURE IF EXISTS rm_user_del ;;
CREATE PROCEDURE rm_user_del(p_username varchar(32))
BEGIN
  DELETE FROM rm_users WHERE username = p_username;
END
;;


/**  procedure rm_user_get
 * Returns the data of a given user.
 * Gets the username of the user as a parameter (type: varchar(32)),
 * and returns one or more records with the data of the users who
 * match the data of the username. Matching is done with LIKE. 
 * It may return nothing if such a user does not exist.
 * The record that is returned has these fields:
 *   username, srvname, expiration, enabled
 */
DROP PROCEDURE IF EXISTS rm_user_get ;;
CREATE PROCEDURE rm_user_get(p_username varchar(32))
BEGIN
  SELECT username, srvname, expiration, enableuser AS enabled
  FROM rm_users
  WHERE username LIKE p_username;
END
;;


/** procedure rm_nas_insert
 * Add a new record in the table 'nas'.
 */
DROP PROCEDURE IF EXISTS rm_nas_insert ;;
CREATE PROCEDURE rm_nas_insert(p_ip VARCHAR(128),
                               p_name VARCHAR(128),
                               p_secret VARCHAR(60),
                               p_description VARCHAR(200))
BEGIN
  ### delete it first, if it already exists
  CALL rm_nas_delete(p_ip);

  ### insert a new record
  INSERT INTO nas
  SET
    nasname     = p_ip,
    shortname   = p_name,
    type        = '2',
    secret      = p_secret,
    community   = 'hsmanager',
    description = p_description; 
END
;;


/** procedure rm_nas_update
 * Update a record in the table 'nas'.
 */
DROP PROCEDURE IF EXISTS rm_nas_update ;;
CREATE PROCEDURE rm_nas_update(p_ip VARCHAR(128),
                               p_name VARCHAR(128),
                               p_secret VARCHAR(60),
                               p_description VARCHAR(200))
BEGIN
  UPDATE nas
  SET
    shortname   = p_name,
    secret      = p_secret,
    description = p_description  
  WHERE nasname = p_ip
    AND community = 'hsmanager';
END
;;


/** procedure rm_nas_delete
 * Delete a record in the table 'nas'.
 */
DROP PROCEDURE IF EXISTS rm_nas_delete ;;
CREATE PROCEDURE rm_nas_delete(p_ip VARCHAR(128))
BEGIN
  DELETE FROM nas 
  WHERE nasname = p_ip
    AND community = 'hsmanager';
END
;;


/** procedure rm_nas_check
 * Used to check whether an IP is already registered in the 
 * nas table. If it is registered, then it returns the IP, 
 * otherwise returns 'not-found'.
 */
DROP FUNCTION IF EXISTS rm_nas_check ;;
CREATE FUNCTION rm_nas_check(p_ip VARCHAR(128))
    RETURNS VARCHAR(128)
    READS SQL DATA
BEGIN
  DECLARE v_ip VARCHAR(64) DEFAULT 'not-found';

  SELECT DISTINCT nasname INTO v_ip 
  FROM nas
  WHERE nasname = p_ip AND community = 'hsmanager';

  RETURN v_ip;
END
;;


/** Set the delimiter of the SQL commands back to semicolon. */
DELIMITER ;

