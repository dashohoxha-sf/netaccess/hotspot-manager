
/**
 * Create a user and assign privileges to be able to execute 
 * the functions and procedures in this file.
 */

USE raddb_name;

GRANT EXECUTE ON FUNCTION user_check 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE user_get 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE user_save 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE user_del 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE service_get 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE service_save 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE service_del 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE change_service_name 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

SET PASSWORD FOR 'raddb_apiuser'@'raddb_allowed_hosts' 
  = PASSWORD('raddb_apipass');

/** Create a user for phpMyAdmin, which can only read the data. */
GRANT SELECT ON raddb_name.* 
  TO 'raddb_apiuser'@'localhost' IDENTIFIED BY 'raddb_apipass';
