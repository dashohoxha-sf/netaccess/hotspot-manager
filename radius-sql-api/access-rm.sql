
/**
 * Grant execute permission to the procedures that access 
 * the Radius Manager tables (such as rm_users, nas, etc.).
 */

USE raddb_name;

GRANT EXECUTE ON PROCEDURE rm_user_save 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE rm_user_del 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE rm_user_get 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE rm_nas_insert 
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE rm_nas_update
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON PROCEDURE rm_nas_delete
  TO 'raddb_apiuser'@'raddb_allowed_hosts';

GRANT EXECUTE ON FUNCTION rm_nas_check
  TO 'raddb_apiuser'@'raddb_allowed_hosts';
