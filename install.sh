#!/bin/bash
### install the application

### go to this dir
cd $(dirname $0)

### include the configuration file
. hsmanager.cfg

### get the application path
app_path=$(pwd)

### compile the translation files
langs="en sq_AL nl it de"
for lng in $langs ; do l10n/msgfmt.sh $lng ; done

### set the superuser password
scripts/su_passwd.sh

### init the database
db/init.sh

### install the radius sql api
radius-sql-api/apply.sh

### create the script that runs 'check.php' daily
file=/etc/cron.daily/hsmanager_check.sh
cat <<EOF > $file
#!/bin/bash
$app_path/check.php
EOF
chmod +x $file

### create a cronjob to run 'scripts/restart_radius.sh' peridically
file=/etc/cron.d/hsmng_restart_radius.cron
cat <<EOF > $file
*/5 * * * *    root  $app_path/scripts/restart_radius.sh
EOF

### set propper ownership to files 
### that need to be accessed by apache
file_list="hsmanager.cfg
           config/const.DB.php
           radius-sql-api/DB.php
           .su/supasswd
           scripts/restart_radius.txt
           $rad_prefix/etc/raddb/huntgroups
           $rad_prefix/etc/raddb/clients.conf"
chgrp apache $file_list
chmod g+w    $file_list

### set propper ownership to dirs 
### that need to be accessed by apache
dir_list="db/backup/
          $hs_config_dir"
mkdir -p $dir_list
chgrp -R apache $dir_list
chmod -R g+w    $dir_list

### fix some paths in the scripts
script_files="scripts/save_huntgroup_entry.sh
              scripts/del_huntgroup_entry.sh
              scripts/restart_radius.sh
              scripts/save_client_entry.sh
              scripts/del_client_entry.sh"
sed -e s#/usr/local/sbin/rc.radiusd#$rad_prefix/sbin/rc.radiusd# \
    -e s#/usr/local/etc/raddb/huntgroups#$rad_prefix/etc/raddb/huntgroups# \
    -e s#/usr/local/etc/raddb/clients.conf#$rad_prefix/etc/raddb/clients.conf# \
    -i $script_files

