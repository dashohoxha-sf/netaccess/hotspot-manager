<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Functions and variables that are global
   * and are used through the whole application.
   */

  /** Don't display the logo: Powered by phpWebApp */
define('hide_webapp_logo', 'true');

/** Execute a shell command, checking it first for invalid input. */
function shell($cmd)
{
  //some basic check for invalid input
  $cmd = ereg_replace(';.*', '', $cmd);
  $cmd = $cmd.' 2>&1';
  $output = shell_exec($cmd);
  //WebApp::debug_msg("<xmp>$output</xmp>",
  //                  "shell(<strong>$cmd</strong>)"); //debug

  return $output;
}

/** Add a new log record in the table 'logs'. */
function log_event($event, $user ='', $domain ='', $details ='')
{
  $query = ("INSERT INTO logs (user, domain, time, event, details) "
            . "VALUES ('$user', '$domain', NOW(), '$event', '$details') ");
  WebApp::execQuery($query);
}

/**
 * Make the difference between the two records that are given
 * as parameters and return it as a string. This string can be
 * used as a log detail, when event is the update of a record.
 * Records are associated arrays with field names as keys.
 */
function rec_diff($old_rec, $new_rec)
{
  $arr_diffs = array();
  while (list($field,$new_val) = each($new_rec))
    {
      $old_val = $old_rec[$field];
      if ($new_val==$old_val)  continue;
      $arr_diffs[] = "$field=$new_val";
    }
  $diffs = implode(', ', $arr_diffs);
  return $diffs;
}

/** Create and return a connection to the radius database. */
function get_radiusdb_connection()
{
  include 'radius-sql-api/DB.php';
  $cnn = new MySQLCnn(RADDB_HOST, RADDB_USER, RADDB_PASS, RADDB_NAME);
  return $cnn;
}
?>