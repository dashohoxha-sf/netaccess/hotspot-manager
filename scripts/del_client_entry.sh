#!/bin/bash
### delete the client with the given ip from the radius clients.conf file

### go to this directory
cd $(dirname $0)

### get the parameters
if [ "$1" = ""  ]
then
  echo "Usage: $0 ip"
  exit 1
fi
ip=$1

### delete the lines that matches the given ip
clients_conf=/usr/local/etc/raddb/clients.conf
sed -e "/^client $ip/,+4d" $clients_conf >/tmp/clients.conf
cat /tmp/clients.conf > $clients_conf
rm /tmp/clients.conf

### schedule a restart for radius
echo "restart radius (deleted client: $ip)" >> restart_radius.txt

