#!/bin/bash
### delete the given NAS and domain from the radius hungroups file

### go to this directory
cd $(dirname $0)

### get the parameters
if [ "$1" = ""  ]
then
  echo "Usage: $0 mac [domain]"
  exit 1
fi
mac=$1
mac=$(echo $mac | tr a-z,: A-Z,-)
domain=$2

### construct the sed pattern
if [ "$domain" = "" ]
then pattern="Called-Station-Id == $mac"
else pattern="^$domain .Called-Station-Id == $mac"
fi

### delete the line that matches this pattern
huntgroups=/usr/local/etc/raddb/huntgroups
sed -e "/$pattern/d" $huntgroups >/tmp/huntgroups
cat /tmp/huntgroups > $huntgroups
rm /tmp/huntgroups

### schedule a restart for radius
echo "restart radius (deleted huntgroup: $domain $mac)" >> restart_radius.txt

