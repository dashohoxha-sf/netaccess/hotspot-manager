#!/bin/bash
### add an entry for the given NAS in the radius clients.conf file

### go to this directory
cd $(dirname $0)

### get the parameters
if [ $# -lt 3  ]
then
  echo "Usage: $0 ip secret name"
  exit 1
fi
ip=$1
secret=$2
name=$3

### first delete it, if it already exists
./del_client_entry.sh $ip

### append an entry at the file clients.conf
clients_conf=/usr/local/etc/raddb/clients.conf
cat <<EOF >> $clients_conf
client $ip {
  secret     = $secret
  shortname  = $name
}

EOF

### schedule a restart for radius
echo "restart radius (added client: $ip $secret $name)" >> restart_radius.txt

