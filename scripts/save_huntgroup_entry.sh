#!/bin/bash
### add a line for the given NAS and domain in the radius hungroups file

### go to this directory
cd $(dirname $0)

### get the parameters
if [ $# -lt 2  ]
then
  echo "Usage: $0 mac domain"
  exit 1
fi
mac=$1
mac=$(echo $mac | tr a-z,: A-Z,-)
domain=$2

### first delete it, if it already exists
./del_huntgroup_entry.sh $mac $domain

### append an entry at the file huntgroups
huntgroups=/usr/local/etc/raddb/huntgroups
echo -e "$domain \tCalled-Station-Id == $mac" >> $huntgroups

### schedule a restart for radius
echo "restart radius (added huntgroup: $domain $mac)" >> restart_radius.txt

