#!/bin/bash
### this script is called periodically as a cron job in order
### to check whether the radius needs to be restarted

### go to this directory
cd $(dirname $0)

### if restart_radius.txt has restart requests, then restart radius
if grep -qs 'restart radius' restart_radius.txt
then
  ### remove restart requests
  sed -n -e '/restart radius/d' -i restart_radius.txt

  ### restart radius
  /usr/local/sbin/rc.radiusd restart
fi
