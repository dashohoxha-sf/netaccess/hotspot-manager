<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once 'global.php';

/** 
 * Checks if the given username and password are valid.
 * Returns true or false.
 */
function valid_user()
{
  $username = $_SERVER["PHP_AUTH_USER"];
  $password = $_SERVER["PHP_AUTH_PW"];

  if ($username=='superuser')
    {
      $passwd = shell('cat .su/supasswd');
      $passwd = trim($passwd);
      $rs = new EditableRS('user_data');
      $rs->addRec(array('password'=>$passwd));
    }
  else
    {
      $query = "SELECT * FROM users WHERE username='$username'";
      $rs = new EditableRS('user_data', $query);
      $rs->Open();
    }
  global $webPage;
  $webPage->addRecordset($rs);

  if ($rs->EOF())  return false; //query returned no records

  $crypted_passwd = $rs->Field('password');
  $valid = ($crypted_passwd == crypt($password, $crypted_passwd));

  return $valid;
}

function authenticate()
{
  header("WWW-Authenticate: Basic realm=\"HotSpot Manager\"");
  header("HTTP/1.0 401 Unauthorized");
  $host = $_SERVER['HTTP_HOST'];
  $file = $_SERVER['SCRIPT_NAME'];
  $url = 'http://'.$host.dirname($file);
  print "
<html>
<head>
  <title>Unauthorized</title>
  <meta http-equiv='refresh' content='2;url=$url'>
</head>
<body>
<h1>Sorry, you cannot access this page.</h1>
</body>
";

  exit;
}

/** add some state variables */
function init_user()
{
  $rs = WebApp::openRS('user_data');
  extract($rs->Fields());
  $username = $_SERVER["PHP_AUTH_USER"];

  WebApp::addSVar('username', $username, 'DB');
  WebApp::addSVar('password', $password, 'DB');

  if ($username=='superuser')
    {
      define('SU', 'true');
      WebApp::addSVar('u_id', '0', 'DB');
      WebApp::addSVar('domains', 'all', 'DB');
    }
  else
    {
      //save some data in the session 
      WebApp::addSVar('u_id', $user_id, 'DB');
      WebApp::addSVar('modules', $modules, 'DB');
      WebApp::addSVar('domains', $domains, 'DB');
      WebApp::addSVar('firstname', $firstname);
      WebApp::addSVar('lastname', $lastname);
    }

  //add state vars domain_nr and domain_filter
  add_domain_vars();
}

/**
 * Add the state variables many_domains and domain_filter.
 * The variable many_domain can be true or false. It is used
 * to display differently several parts of the program that
 * are used for selecting domains.
 * The variable domain_filter is used to filter the nases and clients
 * that are shown to a user, so that he cannot see data except
 * on his domain(s).
 */
function add_domain_vars()
{
  $username = WebApp::getSVar('username');

  if ($username=='superuser')
    {
      WebApp::addSVar('many_domains', 'true', 'DB');
      WebApp::addSVar('domain_filter', '"alldomains"="alldomains"', 'DB');
      return;
    }

  //get an array of the domains of the user
  $domains = WebApp::getSVar('domains');
  $arr = explode("\n", $domains);
  $arr_domains = array();
  for ($i=0; $i < sizeof($arr); $i++)
    {
      $domain = trim($arr[$i]);
      if ($domain=='')  continue;
      $arr_domains[] = "\"$domain\"";
    }

  //set vars many_domains and domain_filter
  if (sizeof($arr_domains)==0)
    {
      $many_domains = 'false';
      $domain_filter = '"domain"="none"';
    }
  else if (sizeof($arr_domains)==1)
    {
      $domain = $arr_domains[0];
      if (strtolower($domain)=='"all"')
        {
          $many_domains = 'true';
          $domain_filter = '"alldomains"="alldomains"';
        }
      else
        {
          $many_domains = 'false';
          $domain_filter = "domain = $domain";          
        }
    }
  else  //more than one domain
    {
      $many_domains = 'true';
      $domain_list = implode(', ', $arr_domains);
      $domain_filter = "domain IN ($domain_list)";
    }

  //add the variables
  WebApp::addSVar('many_domains', $many_domains, 'DB');
  WebApp::addSVar('domain_filter', $domain_filter, 'DB');
}

//authenticate if the user is unknown or not valid
if (!isset($_SERVER['PHP_AUTH_USER']))  authenticate();
 else if (!valid_user())  authenticate();

//the user is authenticated successfully
//add some state variables about him (like u_id, modules, domains, etc.)
init_user();
?>
