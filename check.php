#!/usr/bin/php -q
<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

   /**
    * This script checks the expiration time of the clients, and if it
    * has passed, removes their routers from the table routers in DB.
    * This script should be called periodically by a cron job once
    * a day or so.
    */

   /*
    if ($argc < 2)
    {
    print "Usage: $argv[0] output_dir file.xml \n";
    exit(1);
    }
    $dir = $argv[1];
    $fname = $argv[2];
   */

define("APP_PATH", dirname(__FILE__).'/');
define("APP_URL", './');
include_once "webapp.php";

//cleanup_dangling_routers();
//remove_expired_routers();

exit(0);

/*------------------------ functions -----------------------------*/

/**
 * Delete from 'routers' any mac that does not belong 
 * to any of the clients in the client list (dangling mac).
 */
function cleanup_dangling_routers()
{
  //get a list of all the clients
  $clients = "SELECT username FROM clients";
  $rs = WebApp::execQuery($clients);
  $arr_clients = $rs->getColumn('username');
  $client_list = "'" . implode("', '", $arr_clients) . "'";

  //get a list of the dangling macs (for logging purposes)
  $get_macs = ("SELECT client, mac FROM routers "
               . "WHERE client NOT IN ($client_list)");
  $rs = WebApp::execQuery($get_macs);

  //clean up dangling macs
  $remove_macs = "DELETE FROM routers WHERE client NOT IN ($client_list)";
  WebApp::execQuery($remove_macs);

  //add a log record for each router that was removed
  while (!$rs->EOF())
    {
      $client = $rs->Field('client');
      $mac = $rs->Field('mac');
      $d = "Source=check.php, Client=$client, MAC=$mac, Comment: dangling MAC";
      log_event('-ROUTER', $d);
      $rs->MoveNext();
    }
}

/**
 * Remove from routers the routers (macs) of all the clients 
 * whose expiration time has passed.
 */
function remove_expired_routers()
{
  //get the expired clients
  $clients = "SELECT username FROM clients WHERE expiration_time < NOW()";
  $rs = WebApp::execQuery($clients);

  //if empty, return
  if ($rs->EOF())  return false;

  //convert them to a list
  $arr_clients = $rs->getColumn('username');
  $client_list = "'" . implode("', '", $arr_clients) . "'";

  //get a list of the expired macs (for logging purposes)
  $get_macs = ("SELECT client, mac FROM routers "
               . "WHERE client IN ($client_list)");
  $rs = WebApp::execQuery($get_macs);

  //remove them from routers
  $remove_macs = "DELETE FROM routers WHERE client IN ($client_list)";
  WebApp::execQuery($remove_macs);

  //add a log record for each router that was removed
  while (!$rs->EOF())
    {
      $client = $rs->Field('client');
      $mac = $rs->Field('mac');
      $d = "Source=check.php, Client=$client, MAC=$mac, Comment: expiration";
      log_event('-ROUTER', $d);
      $rs->MoveNext();
    }
}
?>
