<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package naslist
 */
class nasEdit extends formWebObj
{
  /** associated array that contains some default values of the form */
  var $fields = array();

  function init()
  {
    $this->addSVar('mode', 'add');    // add | edit
  }

  /** add the new nas in database */
  function on_add($event_args)
  {
    //check that the number of nases for the domain 
    //does not exceed the limit max_nas_nr
    $domain = $event_args['domain'];
    if ($this->limit_nas_nr_reached($domain))
      {
        $this->fields = $event_args;
        return;
      }

    //check that such an id does not exist in DB
    $nas_id = $event_args['id'];
    if ($this->nas_id_exists($nas_id))  
      {
        $this->fields = $event_args;
        return;
      }

    //check that such an ip does not exist in DB
    $nas_ip = $event_args['ip'];
    if ($this->nas_ip_exists($nas_ip))  
      {
        $this->fields = $event_args;
        return;
      }

    //check that the domain is ok
    $domain = $event_args['domain'];
    if ($this->domain_not_ok($domain))
      {
        $this->fields = $event_args;
        return;
      }

    //add the nas
    $this->insert_record($event_args, 'naslist');

    //add in radius
    $this->add_in_radius($event_args);

    //set the new nas as current nas and change the mode to edit
    WebApp::setSVar('nasList->current_nas', $id);
    $this->setSVar('mode', 'edit');

    //add a log record
    $user = WebApp::getSVar('username');
    $name = $event_args['name'];
    $type = $event_args['type'];
    $details = "id=$nas_id, name=$name, type=$type";
    log_event('+NAS', $user, $domain, $details);
  }

  /** Check that the nas nr of the domain has not exceeded the limit. */
  function limit_nas_nr_reached($domain)
  {
    //get the maximum number of nases for the domain
    $rs = WebApp::openRS("get_max_nas_nr", array('domain'=>$domain));
    $max_nas_nr = $rs->Field('max_nas_nr');

    //if $max_nas_nr is 0 or unset, then there is no limit
    if ($max_nas_nr=='' or $max_nas_nr=='0')  return false;

    //get the number of nases in the given domain
    $rs = WebApp::openRS("get_nas_nr", array('domain'=>$domain));
    $nas_nr = $rs->Field('nas_nr');

    //if nr is less than max_nr, then limit is not reached 
    if ($nas_nr < $max_nas_nr)  return false;

    //limit is reached, display a notification message
    $msg = T_("NAS was not added!\n\n\
The domain 'var_domain' can have up to 'var_max_nas_nr' \n\
NASes and this number has been reached!");
    $msg = str_replace('var_domain', $domain, $msg);
    $msg = str_replace('var_max_nas_nr', $max_nas_nr, $msg);
    WebApp::message($msg);

    return true;
  }

  /** Check that such an id does not exist in DB. */
  function nas_id_exists($id)
  {
    $rs = WebApp::openRS('get_nas_id', array('id'=>$id));
    if ($rs->EOF()) return false;

    //display a notification message
    $msg = T_("The id 'var_id' is already used for \n\
another NAS.  Please choose another id.");
    $msg = str_replace('var_id', $id, $msg);
    WebApp::message($msg);

    return true;
  }

  /** Check that such an ip does not exist in DB. */
  function nas_ip_exists($ip)
  {
    $rs = WebApp::openRS('get_nas_ip', array('ip'=>$ip));
    if ($rs->EOF()) return false;

    //display a notification message
    $msg = T_("The IP 'var_ip' is already used for \n\
another NAS.  Please choose another IP.");
    $msg = str_replace('var_ip', $ip, $msg);
    WebApp::message($msg);

    return true;
  }

  /** Check that the domain is ok. */
  function domain_not_ok($domain)
  {
    //the superuser can modify any domain
    if (SU=='true')  return false;

    //get the user domains
    $domains = WebApp::getSVar('domains');

    //if the user has access on all the domains
    //then the domain is ok
    if (strtolower($domains)=='all')  return false;

    //get an array of domains
    $arr_domains = explode("\n", $domains);
    for ($i=0; $i < sizeof($arr_domains); $i++)
      {
        $arr_domains[$i] = trim($arr_domains[$i]);
      }

    //check that the domain is in the list of the user domains
    if (in_array($domain, $arr_domains))  return false;

    //display a notification message
    $msg = T_("You cannot access the domain 'var_domain'!");
    $msg = str_replace('var_domain', $domain, $msg);
    WebApp::message($msg);

    return true;
  }

  /** delete the current nas */
  function on_delete($event_args)
  {
    //get the data of the current NAS before deleting it
    $nas = WebApp::openRS('current_nas');

    //delete the current NAS
    WebApp::execDBCmd('delete_nas');

    //del from radius
    $this->del_from_radius($nas->Fields());

    //acknowledgment message
    WebApp::message(T_("NAS deleted."));

    //since the current_nas is deleted,
    //set current the first NAS in the list
    $nasList = WebApp::getObject('nasList');
    $nasList->selectFirst();

    //log NAS deletion
    extract($nas->Fields());
    $user = WebApp::getSVar('username');
    $details = "id=$id, name=$name, type=$type, mac=$mac, ip=$ip";
    log_event('-NAS', $user, $domain, $details);
  }

  /** save the changes */
  function on_save($event_args)
  {
    //check that the domain is ok
    $domain = $event_args['domain'];
    if ($this->domain_not_ok($domain))
      {
        $this->fields = $event_args;
        return;
      }

    //get the old NAS record
    $old_nas = WebApp::openRS('current_nas');

    //check that the new ip does not override another one
    $new_ip = trim($event_args['ip']);
    $old_ip = trim($old_nas->Field('ip'));
    if ($new_ip!=$old_ip and $this->nas_ip_exists($new_ip))  
      {
        $this->fields = $event_args;
        $this->fields['ip'] = $old_ip;
        return;
      }

    //update the NAS data
    $record = $event_args;
    $record['id'] = WebApp::getSVar('nasList->current_nas');
    $this->update_record($record, 'naslist', 'id');

    //update in radius
    $this->update_in_radius($old_nas->Fields(), $record);

    //add a log record
    $user = WebApp::getSVar('username');
    $details = rec_diff($old_nas->Fields(), $record);
    log_event('~NAS', $user, $domain, $details);
  }

  /** Add a new record with the same data as the current one. */
  function on_clone($event_args)
  {
    $this->fields = $event_args;
    WebApp::setSVar('nasList->current_nas', UNDEFINED);
  }

  function add_in_radius($nas_fields)
  {
    //get the fields as variables
    extract($nas_fields);
    $id = trim($id);
    $mac = trim($mac);
    $domain = trim($domain);

    //get the radius_secret of the domain
    $rs = WebApp::openRS('get_radius_secret', compact('domain'));
    $radius_secret = $rs->Field('radius_secret');

    //add an entry in huntgroups
    if ($mac!='' and $domain!='')
      $this->shell("scripts/save_huntgroup_entry.sh $mac $domain");

    //add the ip and radius_secret in clients.conf
    $this->shell("scripts/save_client_entry.sh $ip $radius_secret $id");

    //insert it in the table nas
    if (integrate_with_rm)
      {
        $nas_fields['radius_secret'] = $radius_secret;
        $this->save_in_table_nas($nas_fields);
      }
  }

  function del_from_radius($nas_fields)
  {
    //get the fields as variables
    extract($nas_fields);
    $mac = trim($mac);
    $domain = trim($domain);
    $ip = trim($ip);

    //delete the entry from huntgroups
    if ($mac!='' and $domain!='')
      $this->shell("scripts/del_huntgroup_entry.sh $mac $domain");

    //remove also from clients.conf
    $this->shell("scripts/del_client_entry.sh $ip");

    //delete it from the table nas
    if (integrate_with_rm)
      {
        $this->del_from_table_nas($ip);
      }
  }

  function update_in_radius($old_nas, $new_nas)
  {
    //update the entry in huntgroups
    $old_mac = trim($old_nas['mac']);
    $old_domain = trim($old_nas['domain']);
    $new_mac = trim($new_nas['mac']);
    $new_domain = trim($new_nas['domain']);
    if ($new_mac!=$old_mac or $new_domain!=$old_domain)
      {
        $this->del_from_radius($old_nas);
        $this->add_in_radius($new_nas);
      }

    //get the radius_secret of the domain
    $rs = WebApp::openRS('get_radius_secret', array('domain'=>$new_domain));
    $radius_secret = $rs->Field('radius_secret');

    //add the ip and radius_secret in clients.conf
    $new_ip = trim($new_nas['ip']);
    $id = WebApp::getSVar('nasList->current_nas');
    $this->shell("scripts/save_client_entry.sh $new_ip $radius_secret $id");

    //update it in the table nas
    if (integrate_with_rm)
      {
        $old_ip = trim($old_nas['ip']);
        $new_ip = trim($new_nas['ip']);
        if ($new_ip!=$old_ip)  $this->del_from_table_nas($old_ip);
        $new_nas['radius_secret'] = $radius_secret;
        $this->save_in_table_nas($new_nas);
      }
  }

  function onParse()
  {
    //get the current nas from the list of nases
    $nas = WebApp::getSVar('nasList->current_nas');

    if ($nas==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='add')
      {
        $nas_data = $this->pad_record(array(), 'naslist'); 
      }
    else
      {
        $rs = WebApp::openRS('current_nas');
        $nas_data = $rs->Fields();
      }
    $nas_data = array_merge($nas_data, $this->fields);
    WebApp::addVars($nas_data);      
  }

  /** Delete the nas with the given ip from the table nas. */
  function del_from_table_nas($ip)
  {
    $query = "call rm_nas_delete('$ip')";
    $conn = get_radiusdb_connection();
    WebApp::execQuery($query, $conn);
  }

  /** Save the nas data in the table nas (needed by RM). */
  function save_in_table_nas($nas_fields)
  {
    //if the ip is not set, do not save it in the table nas
    $ip = trim($nas_fields['ip']);
    if ($ip=='') return;

    //get a connection to the radius database
    $conn = get_radiusdb_connection();

    //if the record exists, then just update, otherwise insert
    $rs = WebApp::execQuery("select rm_nas_check('$ip') as ip", $conn);
    $sql_proc = ($rs->Field('ip')==$ip ? "rm_nas_update" : "rm_nas_insert");

    //get the parameters of the sql procedure
    $domain = $nas_fields['domain'];
    $domain = strtoupper(trim($domain));
    $nas_name = $nas_fields['name'];
    $name = "HS: $domain $nas_name";
    $secret = $nas_fields['radius_secret'];
    $description = $nas_fields['description'];

    //save the nas data
    $query = "call $sql_proc('$ip','$name','$secret','$description')";
    WebApp::execQuery($query, $conn);
  }

  function shell($cmd)
  {
    $output = shell($cmd);
    if (trim($output)!='')  WebApp::debug_msg($output, $cmd);      
  }
}
?>