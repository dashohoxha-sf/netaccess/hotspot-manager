// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  HotSpot Manager.  HotSpot Manager can be used
  to manage the users of a network of HotSpot access points.

  Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

  HotSpot Manager  is free software;  you can redistribute  it and/or
  modify  it under the  terms of  the GNU  General Public  License as
  published by the Free Software  Foundation; either version 2 of the
  License, or (at your option) any later version.

  HotSpot Manager is distributed in  the hope that it will be useful,
  but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
  General Public License for more details.

  You should have  received a copy of the  GNU General Public License
  along  with   NetAccess;  if  not,  write  to   the  Free  Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
*/


function add()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.nasEdit);
  SendEvent('nasEdit', 'add', event_args);
}

function clone()
{
  var event_args = getEventArgs(document.nasEdit);
  SendEvent('nasEdit', 'clone', event_args);
}

function save()
{
  if (data_not_valid())  return;
  
  var event_args = getEventArgs(document.nasEdit);
  SendEvent('nasEdit', 'save', event_args);
}

function del()
{
  var msg = T_("You are deleting the current NAS.");
  if (!confirm(msg))  return;
  SendEvent('nasEdit', 'delete');
}

/** returns true or false after validating the data of the form */
function data_not_valid()
{
  var form = document.nasEdit;
  var id = form.id.value;
  var domain = form.domain.value;

  //id cannot not be empty
  id = id.replace(/ +/, '');
  if (id=='')
    {
      alert(T_("The field ID cannot be empty."));
      form.id.focus();
      return true;
    }

  //domain cannot not be empty
  domain = domain.replace(/ +/, '');
  if (domain=='')
    {
      alert(T_("The field Domain cannot be empty."));
      form.domain.focus();
      return true;
    }
    
  return false;
}
