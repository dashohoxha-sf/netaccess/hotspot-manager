
DROP TABLE IF EXISTS naslist;
CREATE TABLE naslist (
  id varchar(20) NOT NULL default '',
  name varchar(20) default NULL,
  type varchar(20) default NULL,
  domain varchar(20) default NULL,
  description varchar(100) default NULL,
  mac varchar(20) default NULL,
  ip varchar(20) default NULL,
  gateway varchar(20) default NULL,
  dns1 varchar(20) default NULL,
  dns2 varchar(20) default NULL,
  radius_secret varchar(20) default NULL,
  phone varchar(20) default NULL,
  address varchar(250) default NULL,
  latitude varchar(20) default NULL,
  longitude varchar(20) default NULL,
  notes text,
  PRIMARY KEY  (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

