<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package naslist
   */
class nasList extends WebObject
{
  function init()
  {
    $this->addSVar("current_nas", UNDEFINED);      
    //set current the first nas in the list
    $this->selectFirst();  
  }

  /** set current_nas as the first nas in the list */
  function selectFirst()
  {  
    $rs = WebApp::openRS("selected_nases");
    $first_nas = $rs->Field("id");
    $this->setSVar("current_nas", $first_nas);
  }

  function on_next($event_args)
  {
    $page = $event_args["page"];
    WebApp::setSVar("selected_nases->current_page", $page);
    $this->selectFirst();  
  }

  function on_refresh($event_args)
  {
    //recount the selected nases
    WebApp::setSVar("selected_nases->recount", "true");

    //select the first one in the current page
    $this->selectFirst();
  }

  function on_select($event_args)
  {
    //set current_nas to the selected one
    $rs = WebApp::openRS("get_nas", $event_args);
    if ($rs->EOF())
      {
        $this->selectFirst();   
      }
    else
      {
        $nas = $event_args["id"];
        $this->setSVar("current_nas", $nas);
      }
  }

  function on_add_new_nas($event_args)
  {
    //when the button Add New nas is clicked
    //make current_nas UNDEFINED
    $this->setSVar("current_nas", UNDEFINED);
  }

  function onParse()
  {
    //recount the selected nases
    WebApp::setSVar("selected_nases->recount", "true");
  }
}
?>