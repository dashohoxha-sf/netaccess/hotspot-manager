<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package services
   */
class serviceFilter extends WebObject
{
  function init()
  {
    $this->addSVars( array(
                           'id' => '',
                           'name' => '',
                           'text' => ''
                           ) );
    $this->addSVar('filter_condition', '(1=1)');
  }

  function onParse()
  {
    $this->build_filter_condition();
  }

  function build_filter_condition()
  {
    //get state vars
    extract($this->getSVars());

    $arr_filters = array();
    if ($id != '')
      $arr_filters[] = '(id LIKE "%'.$id.'%")';

    if ($name != '')
      $arr_filters[] = '(name LIKE "%'.$name.'%")';

    if ($text != '')
      $arr_filters[] = '(description LIKE "%'.$text.'%" '
        . 'OR notes LIKE "%'.$text.'%")'; 

    $filter = implode(' AND ', $arr_filters);

    if ($filter=='')  $filter = '1=1';
    $filter = '('.$filter.')';

    $this->setSVar('filter_condition', $filter);
  }  
}
?>