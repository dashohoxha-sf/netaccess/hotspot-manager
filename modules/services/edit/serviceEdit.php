<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package services
 */
class serviceEdit extends formWebObj
{
  function init()
  {
    $this->addSVar('mode', 'add');    // add | edit
  }

  /** add the new service in database */
  function on_add($event_args)
  {
    //check that such a id does not exist in DB
    $id = $event_args['id'];
    $rs = WebApp::openRS('get_service_id', array('id'=>$id));
    if (!$rs->EOF())
      {
        $msg = T_("The id 'var_id' is already used for \n\
 another service. Please choose another id.");
        $msg = str_replace('var_id', $id, $msg);
        WebApp::message($msg);
        return;
      }

    //add the service
    $this->insert_record($event_args, 'services');

    //save service in the radiusdb
    $this->save_service_in_radiusdb($event_args);

    //set the new service as current service and change the mode to edit
    WebApp::setSVar('serviceList->current_service', $id);
    $this->setSVar('mode', 'edit');

    //add a log record
    $user = WebApp::getSVar('username');
    $name = $event_args['name'];
    $details = "id=$id, name=$name";
    log_event('+service', $user, '', $details);
  }

  /** delete the current service */
  function on_delete($event_args)
  {
    //get the data of the current service before deleting it
    $srv = WebApp::openRS('current_service');

    //delete the service
    WebApp::execDBCmd('delete_service');

    //delete the service from the radiusdb
    $this->delete_service_from_radiusdb($srv->Field('id'));

    //acknowledgment message
    WebApp::message(T_("Service deleted."));

    //the current_service is deleted,
    //set current the first service in the list
    $serviceList = WebApp::getObject('serviceList');
    $serviceList->selectFirst();

    //log service deletion
    extract($srv->Fields());
    $user = WebApp::getSVar('username');
    $details = "id=$id, name=$name, rate=$download_rate/$upload_rate";
    log_event('-service', $user, '', $details);
  }

  /** save the changes */
  function on_save($event_args)
  {
    //get the old service record
    $old_srv = WebApp::openRS('current_service');

    //update nas data
    $record = $event_args;
    $this->update_record($record, 'services', 'id');

    //save service in the radiusdb
    $this->save_service_in_radiusdb($event_args);

    //add a log record
    $user = WebApp::getSVar('username');
    $details = rec_diff($old_srv->Fields(), $record);
    log_event('~service', $user, '', $details);
  }

  /** save the service in the radius database */
  function save_service_in_radiusdb($event_args)
  {
    $name = $event_args['id'];
    $down_rate = $event_args['download_rate'];
    $up_rate = $event_args['upload_rate'];
    $query = "call service_save('$name', '$down_rate', '$up_rate')";
    $conn = get_radiusdb_connection();
    WebApp::execQuery($query, $conn);
  }

  /** delete the service from the radius database */
  function delete_service_from_radiusdb($service)
  {
    $query = "call service_del('$service')";
    $conn = get_radiusdb_connection();
    WebApp::execQuery($query, $conn);
  }

  function onParse()
  {
    //get the current service from the list of services
    $service = WebApp::getSVar('serviceList->current_service');

    if ($service==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='add')
      {
        $service_data = $this->pad_record(array(), 'services'); 
      }
    else
      {
        $rs = WebApp::openRS('current_service');
        $service_data = $rs->Fields();
      }
    WebApp::addVars($service_data);      
  }
}
?>