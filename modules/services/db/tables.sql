
DROP TABLE IF EXISTS services;
CREATE TABLE services (
  id varchar(20) NOT NULL default '',
  name varchar(20) default NULL,
  description varchar(100) default NULL,
  download_rate int(11) default NULL,
  upload_rate int(11) default NULL,
  online_time time default NULL,
  download_limit int(11) default NULL,
  upload_limit int(11) default NULL,
  notes varchar(250) default NULL,
  PRIMARY KEY  (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

