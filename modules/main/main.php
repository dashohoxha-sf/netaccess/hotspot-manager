<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package main
   */
class main extends WebObject
{
  function init()
  {
    WebApp::addSVar('module', 'domains/domains.html');
  }

  function onParse()
  {
    //get the menu items that the user can access
    include TPL.'main/menu_items.php';


    //get the selected menu item
    $menu_item = WebApp::getSVar('tabs1::menu->selected_item');

    //check the validity of the selected menu item
    $arr_items = array_keys($menu_items);
    if (!in_array($menu_item, $arr_items))
      {
        $menu_item = $arr_items[0];
        WebApp::setSVar('tabs1::menu->selected_item', $menu_item);
      }

    //select the module according to the selected menu item
    switch ($menu_item)
      {
      default:
      case 'users':
        $module = 'users/users.html';
        break;
      case 'domains':
        $module = 'domains/domains.html';
        break;
      case 'naslist':
        $module = 'naslist/naslist.html';
        break;
      case 'clients':
        $module = 'clients/clients.html';
        break;
      case 'services':
        $module = 'services/services.html';
        break;
      case 'misc':
        $module = 'misc/misc.html';
        break;
      case 'logs':
        $module = 'logs/logs.html';
        break;
      case 'user_settings':
        $module = 'user_settings/user_settings.html';
        break;
      }

    //set variables
    WebApp::setSVar('module', $module);
    WebApp::addGlobalVar('module_title', $menu_items[$menu_item]);
  }

  function onRender()
  {
  }
}
?>