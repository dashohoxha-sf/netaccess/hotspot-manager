<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package domains
   */
class domainList extends WebObject
{
  function init()
  {
    $this->addSVar("current_domain", UNDEFINED);      
    //set current the first domain in the list
    $this->selectFirst();

    $domains = WebApp::getSVar('domains');
    if (SU=='true' or strtolower($domains)=='all') 
      $can_add_domains = 'true';
    else
      $can_add_domains = 'false';
    WebApp::addSVar('user-can-add-domains', $can_add_domains, 'DB');
  }

  /** set current_domain as the first domain in the list */
  function selectFirst()
  {  
    $rs = WebApp::openRS("selected_domains");
    $first_domain = $rs->Field("id");
    $this->setSVar("current_domain", $first_domain);
  }

  function on_next($event_args)
  {
    $page = $event_args["page"];
    WebApp::setSVar("selected_domains->current_page", $page);
    $this->selectFirst();  
  }

  function on_refresh($event_args)
  {
    //recount the selected domains
    WebApp::setSVar("selected_domains->recount", "true");

    //select the first one in the current page
    $this->selectFirst();
  }

  function on_select($event_args)
  {
    //set current_domain to the selected one
    $rs = WebApp::openRS("get_domain", $event_args);
    if ($rs->EOF())
      {
        $this->selectFirst();   
      }
    else
      {
        $domain = $event_args["id"];
        $this->setSVar("current_domain", $domain);
      }
  }

  function on_add_new_domain($event_args)
  {
    //when the button Add New Domain is clicked
    //make current_domain UNDEFINED
    $this->setSVar("current_domain", UNDEFINED);
  }

  function onParse()
  {
    //recount the selected domains
    WebApp::setSVar("selected_domains->recount", "true");
  }
}
?>