<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package  domains
 */
class domainEdit extends formWebObj
{
  /** associated array that contains some default values of the form */
  var $fields = array();

  function init()
  {
    $this->addSVar('mode', 'add');    // add | edit
  }

  /** add the new domain in database */
  function on_add($event_args)
  {
    //check that the user is allowed to add new domains
    $user_can_add_domains = WebApp::getSVar('user-can-add-domains');
    if ($user_can_add_domains!='true')
      {
        $this->fields = $event_args;
        return;
      }

    //check that such an id does not exist in DB
    $id = $event_args['id'];
    $rs = WebApp::openRS('get_domain_id', array('id'=>$id));
    if (!$rs->EOF())
      {
        $msg = T_("The domain id 'var_id' is already used for \n\
another domain. Please choose another id.");
        $msg = str_replace('var_id', $id, $msg);
        WebApp::message($msg);
        $this->fields = $event_args;
        return;
      }

    //add the domain
    $this->insert_record($event_args, 'domains');

    //update the hotspot config file
    $this->save_config_file($event_args);

    //set the new domain as current domain and change the mode to edit
    WebApp::setSVar('domainList->current_domain', $id);
    $this->setSVar('mode', 'edit');

    //add a log record
    $user = WebApp::getSVar('username');
    $name = $event_args['name'];
    $details = "id=$id, name=$name";
    log_event('+domain', $user, $id, $details);
  }

  /** delete the current domain */
  function on_delete($event_args)
  {
    //check that the user is allowed to add new domains
    $user_can_add_domains = WebApp::getSVar('user-can-add-domains');
    if ($user_can_add_domains!='true')
      {
        $this->fields = $event_args;
        return;
      }

    //get the data of the current domain before deleting it
    $rs_domain = WebApp::openRS('current_domain');

    //delete the domain
    WebApp::execDBCmd('delete_domain');

    //the current_domain is deleted,
    //set current the first domain in the list
    $domainList = WebApp::getObject('domainList');
    $domainList->selectFirst();

    //acknowledgment message
    WebApp::message(T_("Domain deleted."));

    //log domain deletion
    extract($rs_domain->Fields());
    $user = WebApp::getSVar('username');
    $details = "id=$id, name=$name";
    log_event('-domain', $user, $id, $details);
  }

  /** save the changes */
  function on_save($event_args)
  {
    //get the old domain record
    $old_domain = WebApp::openRS('current_domain');

    //update domain data
    $record = $event_args;
    $record['id'] = WebApp::getSVar('domainList->current_domain');
    $this->update_record($record, 'domains', 'id');

    //update the hotspot config file
    $this->save_config_file($record);

    //add a log record
    $user = WebApp::getSVar('username');
    $domain = $record['id'];
    $details = rec_diff($old_domain->Fields(), $record);
    log_event('~domain', $user, $domain, $details);
  }

  /** Add a new record with the same data as the current one. */
  function on_clone($event_args)
  {
    $this->fields = $event_args;
    WebApp::setSVar('domainList->current_domain', UNDEFINED);
  }

  function onParse()
  {
    $many_domains = WebApp::getSVar('many_domains');
    if ($many_domains=='false')
      {
        //set the domain of the user as the current domain
        $domain = WebApp::getSVar('domains');
        WebApp::setSVar('domainList->current_domain', $domain);
      }

    //get the current domain from the list of domains
    $domain = WebApp::getSVar('domainList->current_domain');

    if ($domain==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='add')
      {
        $domain_data = $this->pad_record(array(), 'domains');
      }
    else
      {
        $rs = WebApp::openRS('current_domain');
        $domain_data = $rs->Fields();
      }

    $domain_data = array_merge($domain_data, $this->fields);
    WebApp::addVars($domain_data);

    $domain_services = $domain_data['services'];
    $this->add_rs_services($domain_services);
  }

  /** Update the hotspot config file. */
  function save_config_file($domain_fields)
  {
    //get the fields of the domain
    extract($domain_fields);

    //construct config data
    $radserver1 = hs_radius_server1;
    $radserver2 = hs_radius_server2;
    $domain_id = $id;
    $config_data = 
      "uamserver $login_page
radiusserver1 $radserver1
radiusserver2 $radserver2
radiusnasid $domain_id
";

    //append the radius secret
    if (trim($config_data)=='')
      $config_data .= "radiussecret $radius_secret\n";

    //append allowed sites
    $arr_allowed_sites = explode("\n", $allowed_sites);
    for ($i=0; $i < sizeof($arr_allowed_sites); $i++)
      {
        $allowed_site = $arr_allowed_sites[$i];
        $allowed_site = trim($allowed_site);
        if ($allowed_site=='')  continue;
        $config_data .= "uamallowed $allowed_site\n";
      }

    //write to the configuration file
    $fname = hs_config_dir."/$id.cfg";
    $fp = fopen($fname, 'w');
    fputs($fp, $config_data);
    fclose($fp);
  }

  function add_rs_services($domain_services)
  {
    $arr_services = explode(',', $domain_services);
    $rs = WebApp::openRS('services');
    $rs->addCol('checked', 'false');
    while (!$rs->EOF())
      {
        $domain_id = $rs->Field('id');
        if (in_array($domain_id, $arr_services))
          $rs->setFld('checked', 'true');
        $rs->MoveNext();
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>