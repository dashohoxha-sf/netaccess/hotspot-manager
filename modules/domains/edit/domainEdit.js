// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  HotSpot Manager.  HotSpot Manager can be used
  to manage the users of a network of HotSpot access points.

  Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

  HotSpot Manager  is free software;  you can redistribute  it and/or
  modify  it under the  terms of  the GNU  General Public  License as
  published by the Free Software  Foundation; either version 2 of the
  License, or (at your option) any later version.

  HotSpot Manager is distributed in  the hope that it will be useful,
  but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
  General Public License for more details.

  You should have  received a copy of the  GNU General Public License
  along  with   NetAccess;  if  not,  write  to   the  Free  Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function add()
{
  if (data_not_valid())  return;

  var event_args = getEventArgs(document.domainEdit);
  SendEvent('domainEdit', 'add', event_args);
}

function save()
{
  if (data_not_valid())  return;
  
  var event_args = getEventArgs(document.domainEdit);
  var services = getCheckedServices();
  event_args += ';' + 'services=' + services;

  SendEvent('domainEdit', 'save', event_args);
}

function getCheckedServices()
{
  var services = document.domain_services.services;
  var checked_items = new Array;
  for(i=0; services[i]; i++)
    {
      if (services[i].checked)
        {
          checked_items.push(services[i].value);
        }
    }

  return checked_items.join(',');
}

function clone()
{
  var event_args = getEventArgs(document.domainEdit);
  SendEvent('domainEdit', 'clone', event_args);
}

function del()
{
  var msg = T_("You are deleting the current domain.");
  if (!confirm(msg))  return;
  SendEvent('domainEdit', 'delete');
}

/** returns true or false after validating the data of the form */
function data_not_valid()
{
  var form = document.domainEdit;
  var id = form.id.value;

  //id should not be empty, otherwise 
  //the domain cannot be accessed anymore
  id = id.replace(/ +/, '');
  if (id=='')
    {
      alert(T_("Please fill the id field."));
      form.id.focus();
      return true;
    }
    
  return false;
}
