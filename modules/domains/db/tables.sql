
DROP TABLE IF EXISTS domains;
CREATE TABLE domains (
  id varchar(20) NOT NULL,
  name varchar(20) character set utf8 NOT NULL,
  description varchar(50) character set utf8 default NULL,
  services varchar(250) default NULL,
  max_nas_nr int(11) default NULL,
  max_client_nr int(11) default NULL,
  login_page varchar(100) default NULL,
  allowed_sites text,
  radius_secret varchar(64) default NULL,
  notes text character set utf8,
  PRIMARY KEY  (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
