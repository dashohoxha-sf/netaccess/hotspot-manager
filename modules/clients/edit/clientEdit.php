<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once FORM_PATH.'formWebObj.php';

/**
 * @package clients
 */
class clientEdit extends formWebObj
{
  /** associated array that contains some default values of the form */
  var $fields = array();

  function init()
  {
    $this->addSVar('mode', 'add');    // add | edit
  }

  /** add a new client in database */
  function on_add($event_args)
  {
    //check that the number of clients for the domain 
    //does not exceed the limit max_client_nr
    $domain = $event_args['domain'];
    if ($this->limit_client_nr_reached($domain))
      {
        $this->fields = $event_args;
        return;
      }

    //check that such a username does not exist
    $username = $event_args['username'];
    if ($this->username_exists($username))
      {
        $this->fields = $event_args;
        return;
      }
    if ($this->user_exists_in_radius($username))
      {
        $this->fields = $event_args;
        return;
      }

    //check that the domain is ok
    $domain = $event_args['domain'];
    if ($this->domain_not_ok($domain))
      {
        $this->fields = $event_args;
        return;
      }

    //add the client
    $event_args['last_modified'] = date('Y-m-d H:i:s', time());
    $event_args['modified_by'] = WebApp::getSVar('username');
    $this->insert_record($event_args, 'clients');

    //save user in the radiusdb
    $this->save_user_in_radiusdb($event_args);

    //set the new client as current client and change the mode to edit
    WebApp::setSVar('clientList->current_client', $username);
    $this->setSVar('mode', 'edit');

    //add a log record
    $user = WebApp::getSVar('username');
    $firstname = $event_args['firstname'];
    $lastname = $event_args['lastname'];
    $details = "username=$username, name=$firstname $lastname";
    log_event('+client', $user, $domain, $details);
  }

  /** Check that the client nr of the domain has not exceeded the limit. */
  function limit_client_nr_reached($domain)
  {
    //get the maximum number of clients for the domain
    $rs = WebApp::openRS("get_max_client_nr", array('domain'=>$domain));
    $max_client_nr = $rs->Field('max_client_nr');

    //if $max_client_nr is 0 or unset, then there is no limit
    if ($max_client_nr=='' or $max_client_nr=='0')  return false;

    //get the number of clients in the given domain
    $rs = WebApp::openRS("get_client_nr", array('domain'=>$domain));
    $client_nr = $rs->Field('client_nr');

    //if nr is less than max_nr, then limit is not reached 
    if ($client_nr < $max_client_nr)  return false;

    //limit is reached, display a notification message
    $msg = T_("Client was not added!\n\n\
The domain 'var_domain' can have up to 'var_max_client_nr'\n\
clients and this number has been reached!");
    $msg = str_replace('var_domain', $domain, $msg);
    $msg = str_replace('var_max_client_nr', $max_client_nr, $msg);
    WebApp::message($msg);

    return true;
  }

  /** Check that such a username does not exist. */
  function username_exists($username)
  {
    $rs = WebApp::openRS('get_username', array('username'=>$username));
    if ($rs->EOF())  return false;

    //display a notification message
    $msg = T_("The username 'var_username' is already used for \n\
somebody else.  Please choose another username.");
    $msg = str_replace('var_username', $username, $msg);
    WebApp::message($msg);

    return true;
  }

  /** Check that such a user does not exist in radius. */
  function user_exists_in_radius($username)
  {
    $conn = get_radiusdb_connection();
    $query = "select user_check('$username') as username";
    $rs = WebApp::execQuery($query, $conn);
    if ($rs->Field('username') != $username)  return false;

    //display a notification message
    $msg = T_("The username 'var_username' is already present in radius.\n\
Please choose another username.");
    $msg = str_replace('var_username', $username, $msg);
    WebApp::message($msg);

    return true;
  }

  /** Check that the domain is ok. */
  function domain_not_ok($domain)
  {
    //the superuser can modify any domain
    if (SU=='true')  return false;

    //get the user domains
    $domains = WebApp::getSVar('domains');

    //if the user has access on all the domains
    //then the domain is ok
    if (strtolower($domains)=='all')  return false;

    //get an array of domains
    $arr_domains = explode("\n", $domains);
    for ($i=0; $i < sizeof($arr_domains); $i++)
      {
        $arr_domains[$i] = trim($arr_domains[$i]);
      }

    //check that the domain is in the list of the user domains
    if (in_array($domain, $arr_domains))  return false;

    //display a notification message
    $msg = T_("You cannot access the domain 'var_domain'!");
    $msg = str_replace('var_domain', $domain, $msg);
    WebApp::message($msg);

    return true;
  }

  /** delete the current client */
  function on_delete($event_args)
  {
    //get the data of the current client before deleting it
    $client_data = WebApp::openRS('current_client');

    //delete the client
    WebApp::execDBCmd('delete_client');

    //the current_client is deleted,
    //set current the first client in the list
    $clientList = WebApp::getObject('clientList');
    $clientList->selectFirst();

    //save user in the radiusdb
    $this->delete_user_from_radiusdb($client_data->Field('username'));

    //acknowledgment message
    WebApp::message(T_("Client deleted."));

    //log client deletion
    extract($client_data->Fields());
    $user = WebApp::getSVar('username');
    $details = "username=$username, name=$firstname $lastname";
    log_event('-client', $user, $domain, $details);
  }

  /** save the changes */
  function on_save($event_args)
  {
    //check that the domain is ok
    $domain = $event_args['domain'];
    if ($this->domain_not_ok($domain))
      {
        $this->fields = $event_args;
        return;
      }

    //get the old client record
    $old_client = WebApp::openRS('current_client');

    //get the username
    $user = WebApp::getSVar('username');

    //update client data
    $record = $event_args;
    $record['username'] = WebApp::getSVar('clientList->current_client');
    $time = $record['expiration_time'];
    $record['expiration_time'] = date('Y-m-d H:i', strtotime($time));
    $record['last_modified'] = date('Y-m-d H:i:s', time());
    $record['modified_by'] = $user;
    $this->update_record($record, 'clients', 'username');

    //save user in the radiusdb
    $this->save_user_in_radiusdb($event_args);

    //add a log record
    unset($record['last_modified']);
    unset($record['modified_by']);
    $details = rec_diff($old_client->Fields(), $record);
    log_event('~client', $user, $domain, $details);
  }

  /** Add a new record with the same data as the current one. */
  function on_clone($event_args)
  {
    $this->fields = $event_args;
    WebApp::setSVar('clientList->current_client', UNDEFINED);
  }

  function onParse()
  {
    //get the current client from the list of clients
    $client = WebApp::getSVar('clientList->current_client');

    if ($client==UNDEFINED)
      {
        $this->setSVar('mode', 'add');
      }
    else
      {
        $this->setSVar('mode', 'edit');
      }
  }

  function onRender()
  {
    $mode = $this->getSVar('mode');
    if ($mode=='add')
      {
        $client_data = $this->pad_record(array(), 'clients');
        $client_data['expiration_time'] = date('Y-m-d H:i', time());
      }
    else
      {
        $rs = WebApp::openRS('current_client');
        $client_data = $rs->Fields();
      }

    //modify some fields
    $time = $client_data['expiration_time'];
    $client_data['expiration_time'] = date('Y-m-d H:i', strtotime($time));
    $time = $client_data['last_modified'];
    $client_data['last_modified'] = date('Y-m-d H:i', strtotime($time));

    //merge $client_data with $this->fields
    $client_data = array_merge($client_data, $this->fields);

    //add variables for the field values of the form
    WebApp::addVars($client_data);

    //create and add a recordset with the services of the domain
    $domain = $client_data['domain'];
    $service_list = $this->get_service_list($domain);
    WebApp::openRS('services', compact('service_list'));

    //add current_time
    $current_time = date('Y-m-d H:i');
    WebApp::addVar('current_time', $current_time);
  }


  /**
   * Save the client in the radius database.
   * The parameter $user_data is an associated array
   * of the client fields and their values.
   */
  function save_user_in_radiusdb($user_data)
  {
    //get the user data
    extract($user_data);

    //get a connection to the radiusdb
    $conn = get_radiusdb_connection();

    //save the user data in radiusdb
    $query = "call user_save('$username','$password','$service','$domain')";
    WebApp::execQuery($query, $conn);

    //save the user data in the table rm_users (of Radius Manager)
    if (integrate_with_rm)
      {
        $srv_id = '0';
        $name = "$firstname $lastname";
        $query = ("call rm_user_save('$username', '$password', '$srv_id', "
                  . "'$expiration_time', '$name', '$email')");
        WebApp::execQuery($query, $conn);
      }
  }

  /** Delete the client from the radius database. */
  function delete_user_from_radiusdb($user)
  {
    $conn = get_radiusdb_connection();

    WebApp::execQuery("call user_del('$user')", $conn);

    if (integrate_with_rm)
      WebApp::execQuery("call rm_user_del('$user')", $conn);
  }

  /**
   * Returns the list of the services of the domain,
   * in the format: 'service1','service2','service3'
   */
  function get_service_list($domain)
  {
    $rs = WebApp::openRS('get_domain_services', compact('domain'));
    $services = $rs->Field('services');
    $arr_services = explode(',', $services);

    for ($i=0; $i < sizeof($arr_services); $i++)
      {
        $service = $arr_services[$i];
        $arr_services[$i] = "'$service'";
      }

    $service_list = implode(',', $arr_services);
    return $service_list;
  }
}
?>