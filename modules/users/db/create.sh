#!/bin/bash
### create the DB tables of the application

### go to this directory
cd $(dirname $0)

### get the variables
. vars

### create the database table
mysql --host=$host --user=$user --password=$passwd \
      --database=$database < tables.sql
