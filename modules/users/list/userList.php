<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package users
   */
class userList extends WebObject
{
  function init()
  {
    $this->addSVar("current_user", UNDEFINED);      
    //set current the first user in the list
    $this->selectFirst();  
  }

  /** set current_user as the first user in the list */
  function selectFirst()
  {  
    $rs = WebApp::openRS("selected_users");
    $first_user = $rs->Field("user_id");
    $this->setSVar("current_user", $first_user);
  }

  function on_next($event_args)
  {
    $page = $event_args["page"];
    WebApp::setSVar("selected_users->current_page", $page);
    $this->selectFirst();  
  }

  function on_refresh($event_args)
  {
    //recount the selected users
    WebApp::setSVar("selected_users->recount", "true");

    //select the first one in the current page
    $this->selectFirst();
  }

  function on_select($event_args)
  {
    //set current_user to the selected one
    $rs = WebApp::openRS("get_user", $event_args);
    if ($rs->EOF())
      {
        $this->selectFirst();   
      }
    else
      {
        $user = $event_args["user_id"];
        $this->setSVar("current_user", $user);
      }
  }

  function on_add_new_user($event_args)
  {
    //when the button Add New user is clicked
    //make current_user UNDEFINED
    $this->setSVar("current_user", UNDEFINED);
  }

  function onParse()
  {
    //recount the selected users
    WebApp::setSVar("selected_users->recount", "true");
  }
}
?>