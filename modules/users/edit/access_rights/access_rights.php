<?php
  /*
   This file is part of  HotSpot Manager.  HotSpot Manager can be used
   to manage the users of a network of HotSpot access points.

   Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

   HotSpot Manager  is free software;  you can redistribute  it and/or
   modify  it under the  terms of  the GNU  General Public  License as
   published by the Free Software  Foundation; either version 2 of the
   License, or (at your option) any later version.

   HotSpot Manager is distributed in  the hope that it will be useful,
   but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   NetAccess;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package users
   */
class access_rights extends WebObject
{
  function on_save_acc_rights($event_args)
  {
    //trim domains
    $domains = $event_args['domains'];
    $event_args['domains'] = trim($domains);

    //get the old record
    $old_rec = WebApp::openRS('get_user_access_rights');

    //update user data
    WebApp::execDBCmd('update_access_rights', $event_args);

    //add a log record
    $user = WebApp::getSVar('username');
    $details = rec_diff($old_rec->Fields(), $event_args);
    log_event('~user', $user, '', $details);
  }

  function onRender()
  {
    //get the access rights of the user
    $rs = WebApp::openRS('get_user_access_rights');
    $user_modules = $rs->Field('modules');
    $user_domains = $rs->Field('domains');

    //add the variable domains
    WebApp::addVar('domains', $domains);

    //build the recordsets of the modules and add them to the webPage
    include TPL.'main/menu_items.php';

    $rs1 = new EditableRS('modules1');
    $rs2 = new EditableRS('modules2');
    $arr_modules = explode(',', $user_modules);
    $nr = sizeof($menu_items);
    $i=0;
    while ( list($id,$title) = each($menu_items) )
      {
        $checked = (in_array($id, $arr_modules) ? 'true' : 'false');
        if ($i < $nr/2) 
          $rs1->addRec(compact('id', 'title', 'checked'));
        else
          $rs2->addRec(compact('id', 'title', 'checked'));
        $i++;
      }

    global $webPage;
    $webPage->addRecordset($rs1);
    $webPage->addRecordset($rs2);
  }
}
?>