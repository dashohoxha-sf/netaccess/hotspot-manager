// -*-C-*- //tell emacs to use C mode
/*
  This file is part of  HotSpot Manager.  HotSpot Manager can be used
  to manage the users of a network of HotSpot access points.

  Copyright 2008 Dashamir Hoxha, dashohoxha@users.sourceforge.net

  HotSpot Manager  is free software;  you can redistribute  it and/or
  modify  it under the  terms of  the GNU  General Public  License as
  published by the Free Software  Foundation; either version 2 of the
  License, or (at your option) any later version.

  HotSpot Manager is distributed in  the hope that it will be useful,
  but  WITHOUT ANY  WARRANTY; without  even the  implied  warranty of
  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
  General Public License for more details.

  You should have  received a copy of the  GNU General Public License
  along  with   NetAccess;  if  not,  write  to   the  Free  Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function make_new_backup()
{
  SendEvent('backup', 'make_backup');
}

function restore(fname)
{
  SendEvent('backup', 'restore', 'fname='+fname);
}

function delete_file(fname)
{
  var msg = T_("You are deleting the file 'v_fname'.");
  msg = msg.replace(/v_fname/, fname);
  if (confirm(msg))  SendEvent('backup', 'delete', 'fname='+fname);
}

function upload()
{
  var new_win = window.open('', 'upload_message', 'width=200,height=150');
  var file = document.upload.sql_file.value;
  var uploading = T_("Uploading:");
  var please_wait = T_("Please wait...");
  var win_content = "<html>\n"
    + "<head>\n"
    + " <title>" + uploading + " " + file + " </title>\n"
    + " <style>\n"
    + "   body \n"
    + "   { \n"
    + "     background-color: #f8fff8; \n"
    + "     margin: 10px; \n"
    + "     font-family: sans-serif; \n"
    + "     font-size: 10pt; \n"
    + "     color: #000066; \n"
    + "   } \n"
    + "   h1 { font-size: 12pt; color: #000066; } \n"
    + "   h2 { font-size: 10pt; color: #aa0000; } \n"
    + " </style>\n"
    + "</head>\n"
    + "<body>\n"
    + "  <h1>" + uploading + " " + file + " </h1>\n"
    + "  <h2>" + please_wait + "<h2>\n"
    + "</body>\n"
    + "</html>\n";
  new_win.document.write(win_content);

  document.upload.submit();
  SendEvent('backup', 'refresh');
}
